@succ shift={lexec:/main,shift}
lsedit shift=/main
.del 1 $
{with
    :should_tell
    ,1
    ,{with
        :subcmd
        ,{sublist:{sublist:{&arg},1,1, },1,1,=}
        ,{if:{eq:{&subcmd},discard},{lexec:/discard},
            {if:{eq:{&subcmd},resume},{lexec:/resume},
                {with:property_pairs,{subst:{prop!:/shift/.properties,me},;,\r},
                    {with
                        :properties
                        ,{parse:pair,{&property_pairs},{sublist:{&pair},1,1,=}}
                        ,{with:descriptions,{subst:{prop!:/shift/.descriptions,me},;,\r},
                            {if:{eq:{&subcmd},load},{lexec:/load},
                                {if:{not:{&subcmd}},{lexec:/shift},
                                    {if:{lmember:{&properties},{&subcmd}},{lexec:/set},
                                        {if:{lmember:{&descriptions},{&subcmd}},{lexec:/set},
                                            {tell:I don't recognise that property.,me}}}}}}
                }}}}
        ,{null:(Suppress default output of whatever the last expression happened to be.)}
    }
}
.end

lsedit shift=/resume
.del 1 $
{if
    :{not:{propdir:/shift/.stage,me}}
    ,{if
        :{&should_tell}
        ,{tell:You are not currently preparing a shift.,me}
    }
    ,{store:{secs},/shift/.mtime,me}
    {if:{&should_tell},{tell:You direct your attention again to the shift you're preparing.,me}}
}
.end

lsedit shift=/load
.del 1 $
{with
    :forms
    ,{subst:{sublist:{&arg}{lit: },2,2, },;,\r}
    ,{foreach
        :property
        ,{lunion:{&properties},{&descriptions}}
        ,{with
            :value
            ,{fold:accumulator,form,\r{&forms},
                {with:value,{prop!:/shift/{&form}/{&property},me},
                    {if
                        :{&value}
                        ,{if:{&accumulator},{&accumulator}\r{&value},{&value}}
                        ,{if
                            :{strlen:{&value}}
                            ,{if:{strlen:{&accumulator}},{&accumulator},{&value}}
                            ,{&accumulator}
                        }
                    }}}
            ,{if:{not:{strlen:{&value}}},{set:value,{prop!:/shift/.defaults/{&property},me}}}
            ,{store:{&value},/shift/.stage/{&property},me}
        }
    }
    ,{if
        :{&should_tell}
        ,{tell:You begin preparing a shift{if:{&forms}, based on your {commas:{&forms}} form}.,me}
    }
    ,{store:{secs},/shift/.mtime,me}
}
.end

lsedit shift=/set
.del 1 $
{if
    :{with:last_time,{prop!:/shift/.mtime,me}
        ,{if:{strlen:{&last_time}},{ge:{subt:{secs},{&last_time}},900},0}
    }
    ,{if
        :{&should_tell}
        ,{tell
            :The staging area for your shift was last modified more than 15 minutes ago.
            \rUse shift load to reset it or shift resume to keep using it anyway.
            ,me
        }
        ,0
    }
    ,{null
        :{if
            :{not:{propdir:/shift/.stage,me}}
            ,{with:arg,{null}
                ,{with:should_tell,0
                    ,{lexec:/load}
                }
            }
        }
        ,{store:{sublist:{&arg},2,-1,=},/shift/.stage/{&subcmd},me}
        ,{store:{secs},/shift/.mtime,me}
    }yes
}
.end

lsedit shift=/shift
.del 1 $
{if
    :{not:{propdir:/shift/.stage,me}}
    ,{if
        :{&should_tell}
        ,{tell
            :You don't have a shift prepared.
            \rUse shift load or shift <attr>=<text> to start one.
            ,me
        }
    }
    ,{null
        :{foreach
            :pair
            ,{&property_pairs}
            ,{with:src_prop,{sublist:{&pair},1,1,=},
                {with:dest_prop,{sublist:{&pair},2,2,=},
                    {with:value,{prop!:/shift/.stage/{&src_prop},me},
                        {store:{if:{strip:{&value}},{&value},{null}},{&dest_prop},me}}}}
        }
        ,{with
            :combined_desc
            ,{fold
                :accumulator
                ,prop
                ,\r{&descriptions}
                ,{with:value,{prop!:/shift/.stage/{&prop},me},
                    {if:{&value},{if:{&accumulator},{&accumulator}\r{&value},{&value}},{&accumulator}}}
            },
            ,{delprop:/redesc#,me}
            ,{for:i,1,{count:{&combined_desc}},1,
                {store:{sublist:{&combined_desc},{&i}},/redesc#/{&i},me}}
            ,{store:{count:{&combined_desc}},/redesc#,me}
            ,{store:\{eval:\{list:redesc\}\},/_/de,me}
        }
        ,{if:{&should_tell},{tell:You execute the shift.,me}}
        ,{with:should_tell,0,{lexec:/discard}}
    }
}
.end

lsedit shift=/discard
.del 1 $
{if
    :{not:{propdir:/shift/.stage,me}}
    ,{if:{&should_tell},{tell:You don't have a shift prepared.,me}}
    ,{if:{&should_tell},{tell:You stop preparing the shift.,me}}
}
{delprop:/shift/.stage,me}
{delprop:/shift/.mtime,me}
.end

@succ shiftl={null:{with:arg_,{&arg},{with:arg,load {&arg_},{lexec:/main,shift}}},{with:arg,{null},{lexec:/main,shift}}}
