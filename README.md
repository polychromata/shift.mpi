# shift — faster and more flexible shapeshifting

## Limitations and Disclaimers

I wrote this for myself, targeting one particular stack running on one particular MUCK. I don't know how portable it is,
but i expect the answer is “not very”. It is a warrantyless open-source project, and passively maintained at most. I'm
unlikely to put much more work into it—because i want to spend time on the MUCK, not on this code. :) This is also my
first time writing anything in MPI. I'm given to understand that it's quirky and brittle, so the same should be expected
from this program, generally speaking.

## Installation

First create actions (exits) named `shift` and `shiftl` on your character. Remember to restrict them to your use only.

Then copy and paste the contents of the source file into your MUCK client.

You should read it first, so that you can see for yourself that it does what i've claimed and not anything nasty.

I'm sorry.

## Configuration and Usage

`shift` reads configuration from the `/shift` propdir on your player; specifically:
* `/shift/.properties` tells it what to write back to your properties.
* `/shift/.descriptions` tells it what goes in your description, and in what order. (Your description is assumed to be a
  proplist at `/redesc#` plus a pointer to it at `/_/de`—this is not configurable.)

You'll need to set these manually—`shift` doesn't provide a way to do it. Since i personally don't modify them as much
as i shift, this is fine for me.

Let's say you want to maintain the usual species and gender properties, plus one that tracks whether you can fly, and
let's say your MUCK reads these from `/species`, `/sex`, and `/_flight?`, respectively. To set this up in `shift`:

`@set me=/shift/.properties:species=/species;gender=/sex;flighted=/_flight?`

Your description can be made up of several parts, for example:

`@set me=/shift/.descriptions:oneline;description;outfit`

This will gather the `oneline`, `description`, and `outfit` fields, in that order, and put them together to make your
description.

`shift` looks for fields (for both properties and description) under `/shift/<form>`. If a field is not found there, it
will be read from under `/shift/.defaults`; and if it's not there either, it will be deleted when you execute the shift.
If you want a field to have a default, but be deleted for certain forms, set the value under those forms to a single
space.

To start a shift, do **`shift load <form>`** (this _stages_ the shift). You can edit property and description fields in
this state with **`shift <attr>=<text>`**. These edits will be staged, but not saved back to your forms. (Note that
`shift` doesn't display any message for edits.) When you're done, do **`shift`** by itself and everything will be
applied at once.

To quickly shift to a form as saved, without making changes on top of it, you can use **`shiftl <form>`**.

`shift` detects when you have a staged shift that hasn't been edited for more than 15 minutes, and assumes you've
forgotten about it. It will balk if you try to edit it further. Use **`shift resume`** to tell it that was intentional.

If you start preparing a shift, and then change your mind, you can cancel it with **`shift discard`**.
